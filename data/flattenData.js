const nestedArray = [1, [2], [[3]], [[[4]]]]; // flattened data

module.exports = nestedArray  // exporting the nestedArray (array) to the other modules