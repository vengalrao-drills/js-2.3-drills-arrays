// Do NOT use .reduce to complete this function.
// How reduce works: A reduce function combines all elements into a single value going from left to right.
// Elements will be passed one by one into `cb` along with the `startingValue`.
// `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
// `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.

function reduce(elements, cb, startingValue) {
  let previous = startingValue; // giving an initial value for previous

  for (let index = 0; index < elements.length; index++) {
    previous = cb(previous, elements[index]); // iterating through the array. calling a calllback. that passes previous value(which sum is made for previous elements and) cuurently undergoing elements will also be added into the previous
  }
  return previous; // returned the total sumed value
}

module.exports = reduce;
