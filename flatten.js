// OPTIONS
// Flattens a nested array (the nesting can be to any depth).
// Hint: You can solve this using recursion.
// Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];

function flatten(elements) {
  //
  let flattenAns = []; // initialised array :- flattenAns

  function makeItFlat(Array) {
    for (let index = 0; index < Array.length; index++) {
      // checks the current element whether it's a NUmber or object
      if (typeof Array[index] == "object") {
        // if type of object equals to object
        makeItFlat(Array[index]); // then we recursively call makeItFlat
      } else {
        flattenAns.push(Array[index]); //// if number. then we push directlymto flattenAns
      }
    }
  }
  makeItFlat(elements); // calling a function makeItFlat.
  // above function- at first makeItFlat function starts.then it conitnuosly iterate till its length.
  //in between. if any index gets type as object. then it recursively calls the function. till it becomes number.
  return flattenAns; // returns the flatten array
}

module.exports = flatten; // exports the flatten function to other modules
