// Do NOT use .filter, to complete this function.
// Similar to `find` but you will return an array of all elements that passed the truth test
// Return an empty array if no elements pass the truth test

function filter(elements, cb) {
  //  received elements , cb(callbackfunction)
  let filterCheck = []; // initialised an empty array - filterCheck
  for (let index = 0; index < elements.length; index++) {
    // iterating through elements
    if (cb(elements, index)) {
      // called a callback function with element&index. receives a boolean value
      filterCheck.push(elements[index]); // it condition passes. will add into the filterCheck(array)
    }
  }
  return filterCheck; // returning the filterCheck(Array)
}

module.exports = filter; //exporting the filter function to the other modules
