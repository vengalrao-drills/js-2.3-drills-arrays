// Do NOT use .filter, to complete this function.
// Similar to `find` but you will return an array of all elements that passed the truth test
// Return an empty array if no elements pass the truth test

const elements = require("../data/data"); // importing the data - array [1,2,3,4,5,5]
const filter = require("../filter"); // importing the function filter

function testFilter() {
  // first parameter elements- data
  // second parameter callback function
  let finalFilter = filter(elements, (Array, index) => {
    if (Array[index] > 0) {
      // filtering the data that is greater than 0
      return true; // return true - it's a callback function
    }
  });
  return finalFilter; // return the filtered data,  i.e, is greater than 0
}

console.log(testFilter()); // printing the data




// perfomed this operation- in using filter - using callback 
// let t = [1,2,3,4,5]
// t = t.filter((index)=>{
//     return index>0
// })
// console.log(t)
