// Do NOT use .map, to complete this function.
// How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
// Produces a new array of values by mapping each value in list through a transformation function (iteratee).
// Return the new array.

const data = require("../data/data");   // importing the data - array [1,2,3,4,5,5]
const map = require("../map");  // importing map function
// arguments 1st argument data, second callback (it has agrguments index and data).
function testMap() {
  let newData = map(data, (index, data) => {
    return data[index]; 
  });
  return newData; // data will be received in the form of array
}
console.log(testMap()); // receives the data (Array)
