// OPTIONS
// Flattens a nested array (the nesting can be to any depth).
// Hint: You can solve this using recursion.
// Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];

const flattenData = require("../data/flattenData"); // importing the flattenData
const flatten = require("../flatten"); // importing flatten function
function testFlatten() {
  let flattenAnswer = flatten(flattenData); // calling the flatten function- with flattenData
  return flattenAnswer; // returning the received flatted data
}

console.log(testFlatten());
