// CONDITION
// Do NOT use .includes, to complete this function.
// Look through each value in `elements` and pass each element to `cb`.
// If `cb` returns `true` then return that element.
// Return `undefined` if no elements pass the truth test.


const elements = require('../data/data')  // importing the data - array [1,2,3,4,5,5]
const find = require('../find') // importing the find function.  

function testFind(){  
    let findAnswer = find(elements , (Array, index, item)=>{
        if (Array[index]==item){   // returning true if the number is finded.
            return true  
        }
    },  11 )
return findAnswer  // returning the findAnswer array
}

console.log(testFind()) // calling the testFind function and printing it.
