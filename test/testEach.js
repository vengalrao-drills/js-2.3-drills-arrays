// CONDITIONS
// Do NOT use forEach to complete this function.
// Iterates over a list of elements, yielding each in turn to the `cb` function.
// This only needs to work with arrays.
// You should also pass the index into `cb` as the second argument

const data = require("../data/data"); // importing the data - array [1,2,3,4,5,5]
const each = require("../each"); // importing the each function to use ----  each()

// here i need to iterate every index. so in each function

function testEach(data) {
  // first argument is data (array)
  // second argument is callback function
  // in second argument
  // the parameters are index, value
  each(data, (index, value) => {
    console.log(`index :-${index} - ${value} `); // will be printing the index, value. i.e, recived values
  });
}

testEach(data); /// calling the function -testEach with data
