const reduce = require("../reduce")
const elements = require("../data/data");   // importing the data - array [1,2,3,4,5,5]

function testReduce(){
    // passing a 1st argument- elements(array-data) ,
    // 2nd - argument callback. it has { previous, current} arguments
    let reduceNumbers = reduce(elements , (previous , current )=>{
        return previous + current //... it he previous will be added to current(present index passes in array)
    },  0)
    return reduceNumbers // returning the value
}
    console.log(testReduce())
 
