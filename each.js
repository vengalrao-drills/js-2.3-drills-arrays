// CONDITIONS
// Do NOT use forEach to complete this function.
// Iterates over a list of elements, yielding each in turn to the `cb` function.
// This only needs to work with arrays.
// You should also pass the index into `cb` as the second argument
// based off http://underscorejs.org/#each

function each(elements, cb) {
  // Received the elements. Next iterate through the element(Array)
  for (let i = 0; i < elements.length; i++) {
    // now element undergoes callback function
    cb(i, elements[i]); // then the callback function will do it work accordingly- the function is in testEach.js (second paramter)
  }
}

module.exports = each; //exporting the each function to the other modules
