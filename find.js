// CONDITION
// Do NOT use .includes, to complete this function.
// Look through each value in `elements` and pass each element to `cb`.
// If `cb` returns `true` then return that element.
// Return `undefined` if no elements pass the truth test.

function find(elements, cb, findValue) {
  let check; // inititalised a variable name:- check
  for (let index = 0; index < elements.length; index++) {
    // iterating
    check = cb(elements, index, findValue); // callback function,
    if (check == true) {
      // if it return true(if element is found)
      return elements[index]; // then it returns that element
    }
  }
  return `undefined`; // if all the aboe conditions failed. then return undefined.
}

module.exports = find; //exporting the find function to the other modules
